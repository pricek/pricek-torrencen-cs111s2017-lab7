//***********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kierra Price and Natasha Torrence
// CMPSC 111 Spring 2017
// Lab 7
// Date: 3/9/17
//
// Purpose: Debugging a code
//***********************************

import java.util.Date;
import java.util.Scanner;
import java.text.DecimalFormat;

public class CommandLineGeometer {

    private enum GeometricShape { sphere, triangle, cylinder };

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double radius; //declaring variables
        double height;
        double x;
        double y;
        

        System.out.println("Kierra Price and Natasha Torrence " + new Date()); //names and date
        System.out.println("Welcome to the Command Line Geometer!");
        System.out.println();

	GeometricShape shape = GeometricShape.sphere; //declares sphere as the shape

        System.out.println("What is the radius for the " + shape + "?"); //asks user for double
        radius = scan.nextDouble();
        System.out.println();

	//Rounding the output to two decimal places
	DecimalFormat fmt = new DecimalFormat("0.##");	

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius); //calculates volume of sphere
        double sphereVolume = GeometricCalculator.calculateSphereVolume(radius);
        System.out.println("The volume is equal to " + fmt.format(sphereVolume));
        System.out.println();

	System.out.println("Calculating the surface area of a " + shape + " with a radius equal to " + radius); //calculates surface area of sphere
	double sphereSurfaceArea = GeometricCalculator.calculateSphereSurfaceArea(radius);
	System.out.println("The volume is equal to " + fmt.format(sphereSurfaceArea));
	System.out.println();

        shape = GeometricShape.triangle; //declares triangle as the shape

        System.out.println("What is the length of the base?");
        x = scan.nextDouble();

        System.out.println("What is the length of the height?");
        y = scan.nextDouble();

        //calculates area of triangle
        System.out.println("Calculating the volume of a " + shape + " with the base equal to " + x + " and the height equal to " + y);

        System.out.println("Calculating the area of a " + shape);
        double triangleArea = GeometricCalculator.calculateTriangleArea(x, y);
        System.out.println("The area is equal to " + fmt.format(triangleArea));
        System.out.println();

        shape = GeometricShape.cylinder; //declares cylinder as the shape

        System.out.println("What is the radius for the " + shape + "?"); //asks user for radius of cylinder
        radius = scan.nextDouble();
        System.out.println();

        System.out.println("What is the height for the " + shape + "?"); //asks user for height of cylinder
        height = scan.nextDouble();
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius); //calculates volume of cylinder
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height);
        System.out.println("The surface area is equal to " + fmt.format(cylinderVolume));
        System.out.println();

	//calculates surface area of cylinder
	System.out.println("Calculating the surface area of a " + shape + " with radius equal to " + radius + " and a height of " + height); 		double cylinderSurfaceArea = GeometricCalculator.calculateCylinderSurfaceArea(radius, height);
	System.out.println("The volume is equal to " + fmt.format(cylinderSurfaceArea)); 
    }
}
