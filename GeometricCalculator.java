public class GeometricCalculator {
   
    //Volume of a Sphere
    public static double calculateSphereVolume(double radius) {
        double volume1;
        volume1 = (1.3333) * (Math.PI) * radius * radius * radius;
        return volume1;
	}

    //Surface Area of a Sphere
    public static double calculateSphereSurfaceArea(double radius) {
    	double SurfaceArea1;
	SurfaceArea1 = 4 * (Math.PI) * radius * radius;
	return SurfaceArea1;
    }
    //Area of a Triangle
    public static double calculateTriangleArea(double a, double b) {
        double area;
        area = (0.5) * a * b;
        return area;
    }
    //Volume of a Cylinder
    public static double calculateCylinderVolume(double radius, double height) {
        double volume2;
        volume2 = (Math.PI) * radius * radius * height;
        return volume2;
    }
   //Surface Area of a Cylinder
   public static double calculateCylinderSurfaceArea(double radius, double height) {
	double SurfaceArea2;
	SurfaceArea2 = (2 * (Math.PI) * radius * radius) + (2 * (Math.PI) * radius * height);
	return SurfaceArea2;
   }
}
